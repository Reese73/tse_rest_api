const axios = require('axios');
const http = require('http');
const url = require('url');
var bodyParser = require("body-parser");

const express = require('express');
const fs = require('fs');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/API", (req, res) => {
    fs.readFile("index.html", (err, data) => {
        if (err){
            res.writeHead(404)
            res.end("file doesn't exsit")
        } else {
            res.writeHead(200, {
                'content-type' : 'text/html ; charset=utf-8'
            })
            res.end(data)
        }
    })
});

app.get("/API/Players", (req, res) => {
    axios.get('http://localhost:8090/players').then(function (response) {
        res.send(response.data);
    });
});

app.get("/API/Teams", (req, res) => {
    axios.get('http://localhost:8090/teams').then(function (response) {
        res.send(response.data);
    });
});

app.get("/API/AddPlayer", (req, res) => {

    let query = url.parse(req.url, true).query

    params = {
        name: query.p_name,
        role: query.p_role,
        team: query.p_team
    }

    axios.post('http://localhost:8090/players/', params);
    res.send("Player added !");
});

app.get("/API/AddTeam", (req, res) => {

    let query = url.parse(req.url, true).query

    params = {
        name: query.t_name,
    }

    axios.post('http://localhost:8090/teams/', params);
    res.send("Team added !");
});

app.get("/API/delPlayer", (req, res) => {

    let query = url.parse(req.url, true).query

    params = {
        p_id: query.p_id,
    }

    axios.delete(`http://localhost:8090/players/${query.p_id}`)
    res.send("player deleted !");
});

app.get("/API/delTeam", (req, res) => {

    let query = url.parse(req.url, true).query

    params = {
        t_id: query.t_id,
    }

    axios.delete(`http://localhost:8090/teams/${query.t_id}`)
    res.send("Team deleted !");
});

app.listen(8080, () => {
    console.log("server up")
});