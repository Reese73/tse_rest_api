package tse.api.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collection;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import tse.api.rest.dao.PlayerDB;
import tse.api.rest.domain.Player;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class PlayerControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private PlayerDB playerDB;

	@Test
	@Order(1)
	public void testAll() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders
				.get("/players")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.length()", is(2)))
	    		.andExpect(jsonPath("$[?(@.name == 'Alexandre')].role", Matchers.contains("Guardian")));
	}
	
	@Test
	@Order(2)
	public void testOne() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders
				.get("/players/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.name", is("Marc")));
	}
	
	@Test
	@Order(3)
	public void testNewPlayer() throws Exception {
		
		Player player = new Player("Julien", "Defenseur", "Saint-Etienne");
		
		ObjectMapper mapper = new ObjectMapper();
        byte[] PlayerAsBytes = mapper.writeValueAsBytes(player);
		
		mvc.perform(
				MockMvcRequestBuilders
				.post("/players")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(PlayerAsBytes))
				.andExpect(status().isOk());
		
		assertEquals(3, playerDB.count());
		
		Collection<Player> players = playerDB.findAll();
		
		boolean found = false;
		
		for (Player currentPlayer : players) {
			
			if (currentPlayer.getName().equals("Julien")) {
				
				found = true;
				
			}
		}
		
		assertTrue(found);
	}
	
	@Test
	@Order(4)
	public void testDeletePlayer() throws Exception {
		
		Player player = new Player("Remi", "TEST", "Saint-Etienne");
		
		playerDB.save(player);
		
		Collection<Player> players = playerDB.findAll();
		
		Long id = 0L;
		
		for (Player currentPlayer : players) {
			
			if (currentPlayer.getName().equals("Remi")) {
				
				id = currentPlayer.getId();
			}
		}
		
		mvc.perform(
				MockMvcRequestBuilders
				.delete("/players/" + id)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		assertEquals(3, playerDB.count());
	}
	
	@Test
	@Order(5)
	public void testReplacePlayer() throws Exception {
		
		Player player = new Player("Marc", "Avant", "Saint-Etienne");
		
		ObjectMapper mapper = new ObjectMapper();
        byte[] PlayerAsBytes = mapper.writeValueAsBytes(player);
        
        mvc.perform(
				MockMvcRequestBuilders
				.put("/players/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(PlayerAsBytes))
				.andExpect(status().isOk());
        
        player = playerDB.findById(1L).orElse(null);
        
        if (player == null) {
        	
        	fail("Player not found");
        }
        
        assertEquals("Avant", player.getRole());
	}
}

