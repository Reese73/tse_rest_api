package tse.api.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collection;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import tse.api.rest.dao.TeamDB;
import tse.api.rest.domain.Team;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class TeamControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private TeamDB teamDB;

	@Test
	@Order(1)
	public void testAll() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders
				.get("/teams")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.length()", is(2)));
	}
	
	@Test
	@Order(2)
	public void testOne() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders
				.get("/teams/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.name", is("Saint-Etienne")));
	}
	
	@Test
	@Order(3)
	public void testNewTeam() throws Exception {
		
		Team team = new Team("Montailleur");
		
		ObjectMapper mapper = new ObjectMapper();
        byte[] teamAsBytes = mapper.writeValueAsBytes(team);
		
		mvc.perform(
				MockMvcRequestBuilders
				.post("/teams")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(teamAsBytes))
				.andExpect(status().isOk());
		
		assertEquals(3, teamDB.count());
		
		Collection<Team> teams = teamDB.findAll();
		
		boolean found = false;
		
		for (Team currentTeam : teams) {
			
			if (currentTeam.getName().equals("Montailleur")) {
				
				found = true;
				
			}
		}
		
		assertTrue(found);
	}
	
	@Test
	@Order(4)
	public void testDeleteTeam() throws Exception {
		
		Team team = new Team("Grignon");
		
		teamDB.save(team);
		
		Collection<Team> teams = teamDB.findAll();
		
		Long id = 0L;
		
		for (Team currentTeam : teams) {
			
			if (currentTeam.getName().equals("Grignon")) {
				
				id = currentTeam.getId();
			}
		}
		
		mvc.perform(
				MockMvcRequestBuilders
				.delete("/teams/" + id)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		assertEquals(2, teamDB.count());
	}
	
	@Test
	@Order(5)
	public void testReplaceTeam() throws Exception {
		
		Team team = new Team("Chamousset");
		
		ObjectMapper mapper = new ObjectMapper();
        byte[] TeamAsBytes = mapper.writeValueAsBytes(team);
        
        mvc.perform(
				MockMvcRequestBuilders
				.put("/teams/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(TeamAsBytes))
				.andExpect(status().isOk());
        
        team = teamDB.findById(1L).orElse(null);
        
        if (team == null) {
        	
        	fail("Team not found");
        }
        
        assertEquals("Chamousset", team.getName());
	}
}

