package tse.api.rest.controllers;

import org.springframework.web.bind.annotation.RestController;

import tse.api.rest.dao.TeamDB;
import tse.api.rest.domain.Team;
import tse.api.rest.exceptions.TeamNotFoundException;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class TeamController {

	private final TeamDB T_repository;
	
	TeamController(TeamDB T_repository) {
		this.T_repository = T_repository;
	}
	
	@GetMapping("/teams")
	List<Team> all() {
		return T_repository.findAll();
	}
	
	@PostMapping("/teams")
	Team newTeam(@RequestBody Team newTeam) {
	    return T_repository.save(newTeam);
	}
	
	@GetMapping("/teams/{id}")
	Team one(@PathVariable Long id) {

		return T_repository.findById(id).orElseThrow(() -> new TeamNotFoundException(id));
	}
	
	@DeleteMapping("/teams/{id}")
	void deleteTeam(@PathVariable Long id) {
		T_repository.deleteById(id);
	}
	
	@PutMapping("/teams/{id}")
	Team replaceTeam(@RequestBody Team newTeam, @PathVariable Long id) {

		return T_repository.findById(id)
	      .map(team -> {
	        team.setName(newTeam.getName());
	        return T_repository.save(team);
	      })
	      .orElseThrow(() -> new TeamNotFoundException(id));
	}
}
