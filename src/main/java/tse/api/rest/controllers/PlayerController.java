package tse.api.rest.controllers;

import org.springframework.web.bind.annotation.RestController;

import tse.api.rest.dao.PlayerDB;
import tse.api.rest.domain.Player;
import tse.api.rest.exceptions.PlayerNotFoundException;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class PlayerController {

	private final PlayerDB P_repository;
	
	PlayerController(PlayerDB P_repository) {
		this.P_repository = P_repository;
	}
	
	@GetMapping("/players")
	List<Player> all() {
		return P_repository.findAll();
	}
	
	@PostMapping("/players")
	Player newPlayer(@RequestBody Player newPLayer) {
	    return P_repository.save(newPLayer);
	}
	
	@GetMapping("/players/{id}")
	Player one(@PathVariable Long id) {

		return P_repository.findById(id).orElseThrow(() -> new PlayerNotFoundException(id));
	}
	
	@DeleteMapping("/players/{id}")
	void deletePlayer(@PathVariable Long id) {
		P_repository.deleteById(id);
	}
	
	@PutMapping("/players/{id}")
	Player replaceTeam(@RequestBody Player newPlayer, @PathVariable Long id) {

		return P_repository.findById(id)
	      .map(player -> {
	        player.setName(newPlayer.getName());
	        player.setRole(newPlayer.getRole());
	        player.setTeam(newPlayer.getTeam());
	        return P_repository.save(player);
	      })
	      .orElseThrow(() -> new PlayerNotFoundException(id));
	}
}
