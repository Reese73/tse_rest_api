package tse.api.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tse.api.rest.domain.Player;

public interface PlayerDB extends JpaRepository<Player, Long> {

}