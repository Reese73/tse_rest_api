package tse.api.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tse.api.rest.domain.Team;

public interface TeamDB extends JpaRepository<Team, Long> {

}