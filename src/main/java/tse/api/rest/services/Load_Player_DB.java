package tse.api.rest.services;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
import tse.api.rest.dao.PlayerDB;
import tse.api.rest.dao.TeamDB;
import tse.api.rest.domain.Player;


@Configuration
@Slf4j
public class Load_Player_DB {

	@Bean
	CommandLineRunner initPlayerDatabase(PlayerDB P_repository, TeamDB T_repository) {
		return args -> {
			
			log.info("Preloading " + P_repository.save(new Player("Alexandre", "Guardian", "Saint-Etienne")));		
			log.info("Preloading " + P_repository.save(new Player("Loic", "Attaquant", "Saint-Etienne")));
		};
	}
	
}