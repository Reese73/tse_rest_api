package tse.api.rest.services;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import tse.api.rest.dao.TeamDB;
import tse.api.rest.domain.Team;

@Configuration

public class Load_Team_DB {

	@Bean
	CommandLineRunner initTeamDatabase(TeamDB T_repository) {
		return args -> {
			T_repository.save(new Team("Saint-Etienne"));	
			T_repository.save(new Team("FC Albertville"));
		};
	}
	
}