package tse.api.rest.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Player {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
  
	private String name;
	private String role;
	private String team;
  
	
	public Player(String name, String role, String team) {
		this.setName(name);
		this.setRole(role);
		this.setTeam(team);
	}
	
	Player(){
		
	}

}
